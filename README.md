# 云笔记

#### 介绍
用云开发开发的云笔记小程序，主要功能有新建笔记、文档扫描、语音速记、新建日记，帮助大家记录一些重要的事情。

#### 软件架构
[小程序云开发](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)、[微信同声传译插件](https://developers.weixin.qq.com/miniprogram/dev/extended/service/translator.html)、[腾讯云通用印刷体识别](https://console.cloud.tencent.com/ocr/general)

#### 效果截图
笔记列表页面、新建笔记页面、ppt识别结果页面
![输入图片说明](https://images.gitee.com/uploads/images/2020/0406/175428_8f2de03f_4759886.png "QQ截图20200406172043.png")
语言转文字页面、新建日记页面、日记列表页面
![输入图片说明](https://images.gitee.com/uploads/images/2020/0406/175511_0c084c09_4759886.png "QQ截图20200406173300.png")

#### 安装教程

1.  将本项目 clone 到本地，并使用你自己的小程序账号导入项目。 
2.  开通云开发环境，并创建3集合 note、scan、diary
3.  修改 app.js 文件和所有云函数 index.js 文件中的env 为自己的环境ID
4.  在 `公众平台 → 设置 → 第三方服务 → 插件管理` 中 添加微信同声传译插件 (`wx069ba97219f66d99`)
5.  访问腾讯云开通 [通用印刷体识别](https://console.cloud.tencent.com/ocr/general) ，在[API秘钥管理](https://console.cloud.tencent.com/cam/capi) 新建秘钥(就是SecretID和SecretKey)
6.  将 SecretID和SecretKey 填到云函数OCR_Detection/index.js文件中(大概是第16行)
7.  上传部署 cloudfunctions 文件夹下的云函数，右击、上传并部署：云端安装依赖 
8.  可以启动调试看看效果 


#### 更新日志

2020年4月6号第一个版本开发完成


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 许可证
[MIT](https://gitee.com/hzjsj/tcb-hackthon-note/blob/master/LICENSE)


