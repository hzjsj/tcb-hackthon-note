// pages/my/my.js
const db = wx.cloud.database();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    diary:0,
    scan:0,
    note:0
  },

  //弹出赞赏码
  showQrcode() {
    wx.previewImage({
      urls: ['cloud://hzpc.687a-hzpc-1258873690/images/zanshang.png'],
      current: 'cloud://hzpc.687a-hzpc-1258873690/images/zanshang.png' // 当前显示图片的http链接      
    })
  },

  //统计笔记、日记、扫描次数
  getList:function(){
    db.collection('note').count().then(res => {
      this.setData({
        note:res.total
      })
    })
    db.collection('scan').count().then(res => {
      this.setData({
        scan:res.total
      })
    })
    db.collection('diary').count().then(res => {
      this.setData({
        diary:res.total
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})