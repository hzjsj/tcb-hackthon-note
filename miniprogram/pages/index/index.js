const util = require('../../utils/util.js');
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    notes: [],
    title: '',
    skip: 0,
    modalName: null
  },

  // 复制和删除操作
  mores: function (e) {
    let that = this;
    wx.showActionSheet({
      itemList: ['复制', '删除'],
      success(res) {
        if (res.tapIndex == 0) {
          that.getText(e.currentTarget.dataset.text)
        }
        if (res.tapIndex == 1) {
          that.delete(e.currentTarget.dataset.id)
        }
      },
      fail(res) {
        console.log(res.errMsg)
      }
    })
  },
  getText: function (e) {
    wx.setClipboardData({
      data: e,
      success(res) {
        wx.getClipboardData({
          success(res) {
          }
        })
      }
    })
  },
  delete: function (e) {
    let that = this;
    wx.showModal({
      title: '提示',
      content: '确定删除吗?',
      success(res) {
        if (res.confirm) {
          db.collection('note').doc(e).remove()
            .then(res => {
              that.setData({
                skip: 0
              })
              that.getNodes()
            })
            .catch(console.error)
        }
      }
    })
  },

  // 底部弹出显示和隐藏
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },

  // 搜索
  search: function (e) {
    this.setData({
      title: e.detail.value,
      skip: 0
    })
    this.getNodes()
  },

  //云函数获取note集合数据
  getNodes: function (callback) {
    if (!callback) {
      callback = res => { }
    }
    wx.showLoading({
      title: '数据加载中',
    })
    wx.cloud.callFunction({
      // 云函数名称
      name: 'getNotes',
      // 传给云函数的参数
      data: {
        title: this.data.title,
        skip: this.data.skip
      }
    }).then(res => {
      let skip = this.data.skip;
      let notes = this.data.notes;
      let data = res.result.data;

      for (let i = 0; i < data.length; i++) {
        data[i].createTime = util.formatTime(new Date(data[i].create_time))
      }

      this.setData({
        notes: skip == 0 ? data : notes.concat(data)
      }, res => {
        wx.hideLoading()
        callback();
      }
      )
      this.setData({
        skip: skip + 20
      })

    }).catch(console.error)
  },

  //页面跳转操作
  addnote: function () {
    wx.navigateTo({
      url: '../note/note'
    })
  },
  addscan: function () {
    wx.navigateTo({
      url: '../scan/scan'
    })
  },
  addvoice: function () {
    wx.navigateTo({
      url: '../voice/voice'
    })
  },
  adddiary: function () {
    wx.navigateTo({
      url: '../adddiary/adddiary'
    })
  },

  //笔记类型：type=1是富文本编辑，其余文本框编辑
  note: function (e) {
    if (e.currentTarget.dataset.type == 1) {
      wx.navigateTo({
        url: '../note/note?id=' + e.currentTarget.dataset.id
      })
    } else {
      wx.navigateTo({
        url: '../voice/voice?id=' + e.currentTarget.dataset.id
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      skip: 0
    })
    this.getNodes()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      skip: 0
    })
    this.getNodes(res => {
      wx.stopPullDownRefresh();
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getNodes()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})